
import requests
import scrapy
import pyppeteer as pp

from webcrawler.items import ListingItem 
import html2text


class ListingSpider(scrapy.Spider):
    name = 'listing_spider'

    allowed_domains=['bwcon.de']
    start_urls = ['https://www.bwcon.de/aus-dem-netzwerk/meldungen']
    SITE_DOMAIN = 'https://www.bwcon.de'

    fetch_count = 0 
    query_limit = 20

    async def parse(self, response):
        browser = await pp.launch()
        page = await browser.newPage()
        
        await page.goto(self.SITE_DOMAIN + '/aus-dem-netzwerk/meldungen')
       
        url = await page.evaluate('''
            () => document.getElementById('formLoadMore').getAttribute('action') || ''
        ''')

        if url == '':
            await browser.close()

        form = await page.evaluate(
            '''
            () => [...document.querySelectorAll('#formLoadMore input')].reduce((acc, n) => {
                acc[n.getAttribute('name')] = n.getAttribute('value')
                return acc
            }, {})
            '''
        )
       
        current_query_count = 0 
        results = []

        while current_query_count < self.query_limit:
            self.fetch_count = form['tx_bwconlist_bwcon[clickCounter]'] = self.fetch_count + 1
            new_data = requests.post(self.SITE_DOMAIN+url, data=form)
            
            if len(new_data.text) == 0:
                break 

            data = await page.evaluate(
                '''
                () => {
                const parser = new DOMParser()
                const dom = parser.parseFromString(`%s`, 'text/html')

                return [...dom.querySelectorAll('.bwc-panel-content')].reduce((acc, node) => {
                    const h3 = node.querySelector('h3 a')

                    if (h3) {
                    acc.push({
                        href: h3.href,
                    })
                    }

                    return acc
                }, [])
                }
                ''' % new_data.text
            )
            current_query_count += len(data)
            results = results + data
        await browser.close()

        for i in results:
            yield scrapy.Request(i['href'], self.process_item,cb_kwargs={"url":i['href']})        

    async def process_item(self,response,url):
        """method to process a scraped listing item"""

        data_container_path = '//*[@id="c3235"]/div/div[1]/div/div'
        publication_date_path = '//*[@id="c3235"]/div/div[1]/div/div/div/div/div/div/div/div[1]/div/div/text()'
        title_path = '//*[@id="c3235"]/div/div[1]/div/div/div/div/div/div/div/h3/text()'
        description_path = '//*[@id="c3235"]/div/div[1]/div/div/div/div/div/div/div/p/text()'
        body_path = '//*[@id="c3235"]/div/div[1]/div/div/div/div/div/article'

        result = scrapy.Selector(response).xpath(data_container_path)        
        listing_instance = ListingItem()
        listing_instance['url'] = url 
        listing_instance['title'] = result.xpath(title_path).extract_first(None)
        listing_instance['publication_date'] = result.xpath(publication_date_path).extract_first(None)
        listing_instance['description'] = result.xpath(description_path).extract_first(None)
        listing_instance['body'] = result.xpath(body_path).extract_first(None)
        if listing_instance['body']:
            listing_instance['body'] = html2text.html2text(listing_instance['body'],bodywidth=0)
        print(listing_instance)
        yield listing_instance
        

   
    