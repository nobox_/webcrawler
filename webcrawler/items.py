import scrapy

class ListingItem(scrapy.Item):
    """item used to represent the scraped data"""
    title = scrapy.Field()
    url = scrapy.Field()
    publication_date = scrapy.Field()
    description = scrapy.Field()
    body = scrapy.Field()
